/****************************************************************************************************************************************

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Based on the work of:
	- Federico Mammano
	- Tom Heath

******************************************************************************************************************************************/

// Inladen van instellingen/standaardwaarden 

#include <Windows.h>

int setInt = GetPrivateProfileInt(L"GLOBAL_SETTINGS", L"SETTINGS_CREATED", 0, L"./settings.ini");
bool settingsCreated = (setInt != 0);

int cam_0_num = GetPrivateProfileInt(L"CAMERA_SETTINGS", L"WEBCAM_0_DEVICE_NUMBER", 0, L"./settings.ini");
int cam_0_align = GetPrivateProfileInt(L"CAMERA_SETTINGS", L"WEBCAM_0_VERT_ORIENTATION", 0, L"./settings.ini");
bool cam_0_vert = (cam_0_align != 0);
float cam_0_fov = (float)GetPrivateProfileInt(L"CAMERA_SETTINGS", L"WEBCAM_0_HMD_FOV_RATIO", 100, L"./settings.ini") / (float)100;

int cam_1_num = GetPrivateProfileInt(L"CAMERA_SETTINGS", L"WEBCAM_1_DEVICE_NUMBER", 1, L"./settings.ini");
int cam_1_align = GetPrivateProfileInt(L"CAMERA_SETTINGS", L"WEBCAM_1_VERT_ORIENTATION", 0, L"./settings.ini");
bool cam_1_vert = (cam_1_align != 0);;
float cam_1_fov = (float)GetPrivateProfileInt(L"CAMERA_SETTINGS", L"WEBCAM_1_HMD_FOV_RATIO", 100, L"./settings.ini") / (float)100;

//----------------------------------------------------------------------------------------------------------------------------------------
// Include Headers (Oculus SDK - Utilities - Webcam)

#include "Win32_DX11AppUtil.h"         
#include "OVR_CAPI_D3D.h"                 
#include <Kernel/OVR_System.h>               
#include "RiftStreamer_WebCam.h"			   

using namespace OVR;

//----------------------------------------------------------------------------------------------------------------------------------------
// Oculus Applicatie
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int)
{
	// Controle instellingen - Maak settings.ini aan indien niet bestaand

	initSettings(settingsCreated);

	//------------------------------------------------------------------------------------------------------------------------------------
	// Initialisatie Oculus Rift en SDK


	OVR::System::Init(OVR::Log::ConfigureDefaultLog(OVR::LogMask_All));

	if (!ovr_Initialize()) { MessageBoxA(NULL, "Unable to initialize libOVR.", "", MB_OK); return 0; }
	ovrHmd HMD = ovrHmd_Create(0);
	if (HMD == NULL)
	{
		HMD = ovrHmd_CreateDebug(ovrHmd_DK2); //Indien geen fysieke rift -> debug modus
	}

	if (!HMD) { MessageBoxA(NULL, "Oculus Rift not detected.", "", MB_OK); ovr_Shutdown(); return 0; }
	if (HMD->ProductName[0] == '\0') MessageBoxA(NULL, "Rift detected, display not enabled.", "", MB_OK);

	bool windowed = (HMD->HmdCaps & ovrHmdCap_ExtendDesktop) ? false : true;
	if (!Platform.InitWindowAndDevice(hInst, Recti(HMD->WindowsPos, HMD->Resolution), windowed))
		return 0;

	//------------------------------------------------------------------------------------------------------------------------------------
	// Aanmaken buffers voor beelden

	ImageBuffer * eyeRenderTexture[2];
	ImageBuffer   * eyeDepthBuffer[2];
	for (int eye = 0; eye < 2; eye++)
	{
		Sizei idealSize = ovrHmd_GetFovTextureSize(HMD, (ovrEyeType)eye,
			HMD->DefaultEyeFov[eye], 1.0f);
		eyeRenderTexture[eye] = new ImageBuffer(true, false, idealSize);
		eyeDepthBuffer[eye] = new ImageBuffer(true, true, eyeRenderTexture[eye]->Size);
	}

	//------------------------------------------------------------------------------------------------------------------------------------
	// Configuratie DirectX

	ovrD3D11Config config;
	config.D3D11.Header.API = ovrRenderAPI_D3D11;
	config.D3D11.Header.BackBufferSize = HMD->Resolution;
	config.D3D11.Header.Multisample = 1;
	config.D3D11.pDevice = Platform.Device;
	config.D3D11.pDeviceContext = Platform.Context;
	config.D3D11.pBackBufferRT = Platform.BackBufferRT;
	config.D3D11.pSwapChain = Platform.SwapChain;

	//------------------------------------------------------------------------------------------------------------------------------------
	// Configuratie Renderen

	ovrEyeRenderDesc EyeRenderDesc[2];
	ovrHmd_ConfigureRendering(HMD, &config.Config,
		ovrDistortionCap_Vignette | ovrDistortionCap_TimeWarp |
		ovrDistortionCap_Overdrive, HMD->DefaultEyeFov, EyeRenderDesc);

	//------------------------------------------------------------------------------------------------------------------------------------
	// Koppel applicatie aan venster

	ovrHmd_SetEnabledCaps(HMD, ovrHmdCap_LowPersistence | ovrHmdCap_DynamicPrediction);
	ovrHmd_AttachToWindow(HMD, Platform.Window, NULL, NULL);

	//------------------------------------------------------------------------------------------------------------------------------------
	// Start webcams

	WebCamManager WebCamMngr(HMD);

	//------------------------------------------------------------------------------------------------------------------------------------	
	// Main applicatie lus

	while (!(Platform.Key['Q'] && Platform.Key[VK_CONTROL]) && !Platform.Key[VK_ESCAPE])
	{
		// Berichtafhandeling + Start Weergave

		Platform.HandleMessages();
		ovrHmd_BeginFrame(HMD, 0);

		//--------------------------------------------------------------------------------------------------------------------------------

		//Oogpositie met IPD correctie

		ovrVector3f ViewOffset[2] = { EyeRenderDesc[0].HmdToEyeViewOffset, EyeRenderDesc[1].HmdToEyeViewOffset };
		ovrPosef EyeRenderPose[2];
		ovrHmd_GetEyePoses(HMD, 0, ViewOffset, EyeRenderPose, NULL);

		//--------------------------------------------------------------------------------------------------------------------------------

		// Afhandeling gezondheidswaarschuwing

		if (Platform.IsAnyKeyPressed())
			ovrHmd_DismissHSWDisplay(HMD);

		//--------------------------------------------------------------------------------------------------------------------------------

		// Koppel beelden webcam

		WebCamMngr.Update();

		//--------------------------------------------------------------------------------------------------------------------------------

		// Renderen naar Rift

		for (int eye = 0; eye < 2; eye++)
		{
			eyeRenderTexture[eye]->SetAndClearRenderSurface(eyeDepthBuffer[eye]);
			WebCamMngr.DrawLookThrough(eye);
		}

		//--------------------------------------------------------------------------------------------------------------------------------

		// Distortion effect

		ovrD3D11Texture eyeTex[2];
		for (int i = 0; i < 2; i++)
		{
			eyeTex[i].D3D11.Header.API = ovrRenderAPI_D3D11;
			eyeTex[i].D3D11.Header.TextureSize = eyeRenderTexture[i]->GetSize();
			eyeTex[i].D3D11.Header.RenderViewport = Recti(Vector2i(0, 0), eyeRenderTexture[i]->GetSize());
			eyeTex[i].D3D11.pTexture = eyeRenderTexture[i]->Tex;
			eyeTex[i].D3D11.pSRView = eyeRenderTexture[i]->TexSv;
		}

		//--------------------------------------------------------------------------------------------------------------------------------

		// Vereiste SDK

		ovrHmd_EndFrame(HMD, EyeRenderPose, &eyeTex[0].Texture);

		//--------------------------------------------------------------------------------------------------------------------------------


	}
	//------------------------------------------------------------------------------------------------------------------------------------	
	// Stop Webcams

	WebCamMngr.StopCapture();

	//------------------------------------------------------------------------------------------------------------------------------------	
	// Sluit applicatie af

	ovrHmd_Destroy(HMD);
	ovr_Shutdown();
	Platform.ReleaseWindow(hInst);

	return(0);

	//------------------------------------------------------------------------------------------------------------------------------------
}
//----------------------------------------------------------------------------------------------------------------------------------------