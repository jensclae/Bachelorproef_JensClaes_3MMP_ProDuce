/****************************************************************************************************************************************

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Based on the work of:
- Federico Mammano
- Tom Heath

******************************************************************************************************************************************/

// Koppel waarden uit settings.ini

#define WEBCAM_NB						1		// Uitzondering -> waarde moet bekend zijn voor compiler
#define WEBCAM_0_DEVICE_NUMBER			cam_0_num		
#define WEBCAM_0_VERT_ORIENTATION		cam_0_vert	
#define WEBCAM_0_HMD_FOV_RATIO			cam_0_fov	
#define WEBCAM_1_DEVICE_NUMBER			cam_1_num		
#define WEBCAM_1_VERT_ORIENTATION		cam_1_vert	
#define WEBCAM_1_HMD_FOV_RATIO			cam_1_fov	

//----------------------------------------------------------------------------------------------------------------------------------------
// Include OpenCV

#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc/imgproc.hpp>

//----------------------------------------------------------------------------------------------------------------------------------------
// Include Oculus SDK onderdelen

#include "Kernel/OVR_Log.h"
#include "Kernel/OVR_Threads.h"

using namespace OVR;
//----------------------------------------------------------------------------------------------------------------------------------------
// Include hand tracker

#include "Hand.h"

//----------------------------------------------------------------------------------------------------------------------------------------
// Declaratie variabele aspect ratio

float fHMDEyeAspectRatio = 0.888889f;

//----------------------------------------------------------------------------------------------------------------------------------------
// Definitie model vierkant

Model::Vertex SimpleQuadVertices[] = { { Vector3f(-1.0f, -1.0f, 0.0f), Model::Color(255, 255, 255, 200), 0.0f, 0.0f },
{ Vector3f(1.0f, -1.0f, 0.0f), Model::Color(255, 255, 255, 200), 1.0f, 0.0f },
{ Vector3f(1.0f, 1.0f, 0.0f), Model::Color(255, 255, 255, 200), 1.0f, 1.0f },
{ Vector3f(-1.0f, 1.0f, 0.0f), Model::Color(255, 255, 255, 200), 0.0f, 1.0f } };
uint16_t SimpleQuadIndices[] = { 0, 1, 2, 3, 0, 2 };

//----------------------------------------------------------------------------------------------------------------------------------------
// Vervagende rand

float fBandWidth = 0.1f;
float fBandHeight = 0.1f;
Model::Vertex FadingEdgeQuadVertices[] = {
	{ Vector3f(-1.0f, -1.0f, 0.0f), Model::Color(255, 255, 255, 0), 0.0f, 0.0f },
	{ Vector3f(-1.0f + fBandWidth, -1.0f, 0.0f), Model::Color(255, 255, 255, 0), 0.5f*fBandWidth, 0.0f },
	{ Vector3f(-1.0f + fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(-1.0f, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 0.0f, 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, -1.0f, 0.0f), Model::Color(255, 255, 255, 0), 0.5f*fBandWidth, 0.0f },
	{ Vector3f(1.0f - fBandWidth, -1.0f, 0.0f), Model::Color(255, 255, 255, 0), 1.0f - 0.5f*fBandWidth, 0.0f },
	{ Vector3f(1.0f - fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, -1.0f, 0.0f), Model::Color(255, 255, 255, 0), 1.0f - 0.5f*fBandWidth, 0.0f },
	{ Vector3f(1.0f, -1.0f, 0.0f), Model::Color(255, 255, 255, 0), 1.0f, 0.0f },
	{ Vector3f(1.0f, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 1.0f, 0.5f*fBandHeight },
	{ Vector3f(-1.0f, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 0.0f, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(-1.0f, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 0.0f, 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 0.5f*fBandHeight },
	{ Vector3f(1.0f, -1.0f + fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 1.0f, 0.5f*fBandHeight },
	{ Vector3f(1.0f, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 1.0f, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(-1.0f, 1.0f, 0.0f), Model::Color(255, 255, 255, 0), 0.0f, 1.0f },
	{ Vector3f(-1.0f, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 0.0f, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(-1.0f + fBandWidth, 1.0f, 0.0f), Model::Color(255, 255, 255, 0), 0.5f*fBandWidth, 1.0f },
	{ Vector3f(-1.0f + fBandWidth, 1.0f, 0.0f), Model::Color(255, 255, 255, 0), 0.5f*fBandWidth, 1.0f },
	{ Vector3f(-1.0f + fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(1.0f - fBandWidth, 1.0f, 0.0f), Model::Color(255, 255, 255, 0), 1.0f - 0.5f*fBandWidth, 1.0f },
	{ Vector3f(1.0f - fBandWidth, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 255), 1.0f - 0.5f*fBandWidth, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(1.0f, 1.0f - fBandHeight, 0.0f), Model::Color(255, 255, 255, 0), 1.0f, 1.0f - 0.5f*fBandHeight },
	{ Vector3f(1.0f, 1.0f, 0.0f), Model::Color(255, 255, 255, 0), 1.0f, 1.0f },
	{ Vector3f(1.0f - fBandWidth, 1.0f, 0.0f), Model::Color(255, 255, 255, 0), 1.0f - 0.5f*fBandWidth, 1.0f }
};
uint16_t FadingEdgeQuadIndices[] = { 0, 1, 2, 3, 0, 2,
4, 5, 6, 7, 4, 6,
8, 9, 10, 11, 8, 10,
12, 13, 14, 15, 12, 14,
16, 17, 18, 19, 16, 18,
20, 21, 22, 23, 20, 22,
24, 25, 26, 27, 24, 26,
28, 29, 30, 31, 28, 30,
32, 33, 34, 35, 32, 34 };
//----------------------------------------------------------------------------------------------------------------------------------------
// Shader

D3D11_INPUT_ELEMENT_DESC QuadVertDesc[] = { { "Position", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Model::Vertex, Pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
{ "Color", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, offsetof(Model::Vertex, C), D3D11_INPUT_PER_VERTEX_DATA, 0 },
{ "TexCoord", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(Model::Vertex, U), D3D11_INPUT_PER_VERTEX_DATA, 0 } };
char* VertexShaderSrc = "float4x4 Proj, View;"
"void main(in  float4 Position  : POSITION,    in  float4 Color : COLOR0, in  float2 TexCoord  : TEXCOORD0,"
"          out float4 oPosition : SV_Position, out float4 oColor: COLOR0, out float2 oTexCoord : TEXCOORD0)"
"{ oPosition = mul(Proj, mul(View, Position)); oTexCoord = TexCoord; oColor = Color; }";
char* PixelShaderSrc = "Texture2D Texture   : register(t0); SamplerState Linear : register(s0);"
"float4 main(in float4 Position : SV_Position, in float4 Color: COLOR0, in float2 TexCoord : TEXCOORD0) : SV_Target"
"{ return Color * Texture.Sample(Linear, TexCoord); }";

//----------------------------------------------------------------------------------------------------------------------------------------
//  WebCamDevice Class

class WebCamDevice
{
private:

	cv::Mat						Frame;
	cv::VideoCapture			Video;
	OVR::Thread					CaptureFrameThread;
	OVR::Mutex				   *pMutex;
	int							iStatus;
	bool						bHasCapturedFrame;
	bool						bIsVertOriented;
	ImageBuffer					*pImageBuffer;
	ShaderFill				   *pShaderFill;
	Model					   *pQuadModel;
	Model					   *pFadingEdgeQuadModel;

	Ptr<ID3D11BlendState>		BlendState;
	unsigned char			   *pucRGBAData;														
public:

	int							iWidth, iHeight, iBufferSize, iColorChannels;
	float						fAspectRatio;
	float						fWebCamHMD_DiagonalFOVRatio;
public:

	WebCamDevice() : iStatus(OVR::Thread::NotRunning), pMutex(NULL), bHasCapturedFrame(false),
		pImageBuffer(NULL), pShaderFill(NULL), pQuadModel(NULL), pFadingEdgeQuadModel(NULL)
	{
		iColorChannels = 4;										
		pucRGBAData = NULL;

	}

	~WebCamDevice()
	{
	}

	int Initialize(int iDeviceNum, bool bVOriented = false, float fDiagonalFOVRatio = 1.0f)
	{
		Video.open(iDeviceNum);															
		if (!Video.isOpened())
		{
			char msg[100];
			sprintf_s(msg, 100, "Cannot open the video of WebCam number %d", iDeviceNum);
			MessageBoxA(NULL, msg, "", MB_OK);
			return(0);
		}
		iWidth = (int)Video.get(CV_CAP_PROP_FRAME_WIDTH);														
		iHeight = (int)Video.get(CV_CAP_PROP_FRAME_HEIGHT);														
		iBufferSize = iWidth*iHeight*iColorChannels;
		fAspectRatio = (float)iHeight / (float)iWidth;
		bIsVertOriented = bVOriented;																					
		fWebCamHMD_DiagonalFOVRatio = fDiagonalFOVRatio;

		pImageBuffer = new ImageBuffer(false, false, Sizei(iWidth, iHeight));

		D3D11_TEXTURE2D_DESC dsDesc;
		dsDesc.Width = iWidth;
		dsDesc.Height = iHeight;
		dsDesc.MipLevels = 1;
		dsDesc.ArraySize = 1;
		dsDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		dsDesc.SampleDesc.Count = 1;
		dsDesc.SampleDesc.Quality = 0;
		dsDesc.Usage = (true ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT);
		dsDesc.CPUAccessFlags = (true ? D3D11_CPU_ACCESS_WRITE : 0);
		dsDesc.MiscFlags = 0;
		dsDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		Platform.Device->CreateTexture2D(&dsDesc, NULL, &pImageBuffer->Tex);
		Platform.Device->CreateShaderResourceView(pImageBuffer->Tex, NULL, &pImageBuffer->TexSv);

		D3D11_BLEND_DESC bm;
		memset(&bm, 0, sizeof(bm));
		bm.RenderTarget[0].BlendEnable = true;
		bm.RenderTarget[0].BlendOp = bm.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bm.RenderTarget[0].SrcBlend = bm.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
		bm.RenderTarget[0].DestBlend = bm.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
		bm.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		Platform.Device->CreateBlendState(&bm, &BlendState.GetRawRef());


		pShaderFill = new ShaderFill(QuadVertDesc, 3, VertexShaderSrc, PixelShaderSrc, pImageBuffer, 0);

		pQuadModel = new Model(Vector3f(0, 0, 0), pShaderFill);

		int iNbVertices = sizeof(SimpleQuadVertices) / sizeof(SimpleQuadVertices[0]);
		for (int i = 0; i < iNbVertices; i++) { pQuadModel->AddVertex(SimpleQuadVertices[i]); }

		int iNbIndices = sizeof(SimpleQuadIndices) / sizeof(SimpleQuadIndices[0]);
		for (int i = 0; i < iNbIndices; i++) { pQuadModel->AddIndex(SimpleQuadIndices[i]); }

		pQuadModel->AllocateBuffers();

		pFadingEdgeQuadModel = new Model(Vector3f(0, 0, 0), pShaderFill);

		iNbVertices = sizeof(FadingEdgeQuadVertices) / sizeof(FadingEdgeQuadVertices[0]);
		for (int i = 0; i < iNbVertices; i++) { pFadingEdgeQuadModel->AddVertex(FadingEdgeQuadVertices[i]); }

		iNbIndices = sizeof(FadingEdgeQuadIndices) / sizeof(FadingEdgeQuadIndices[0]);
		for (int i = 0; i < iNbIndices; i++) { pFadingEdgeQuadModel->AddIndex(FadingEdgeQuadIndices[i]); }

		pFadingEdgeQuadModel->AllocateBuffers();

		CaptureFrameThread = OVR::Thread((OVR::Thread::ThreadFn)&WebCamDevice::CaptureFrameThreadFn, this);
		iStatus = OVR::Thread::Running;
		CaptureFrameThread.Start(OVR::Thread::Running);
		pMutex = new OVR::Mutex();

		return(1);
	}

	void StopCapture()
	{
		if (iStatus == OVR::Thread::NotRunning) { return; }

		iStatus = OVR::Thread::NotRunning;
		CaptureFrameThread.Join();
		Video.release();

		if (pucRGBAData) { OVR_FREE(pucRGBAData); pucRGBAData = NULL; }

		if (pMutex) { delete pMutex; }
	}

	void ProcessHand(cv::Mat &InFrame)
	{
		Hand hand;
		cv::Mat SkinMask(iHeight, iWidth, CV_8UC1);
		hand.BGR2SkinMask(InFrame, SkinMask);
		if (hand.ExtractContourAndHull(SkinMask))
		{
			hand.IdentifyProperties();
			hand.Draw(InFrame);
			//DWORD dwGesture = hand.GestureDetection();
			/*

			CODE WORDT UITGEVOERD BIJ HERKENDE HANDBEWEGING

			*/
		}
	}

	void SetFrame(const cv::Mat &InFrame)
	{
		OVR::Mutex::Locker Locker(pMutex);
		Frame = InFrame;
		bHasCapturedFrame = true;
	}

	bool GetFrame(cv::Mat &OutFrame)
	{
		if (!bHasCapturedFrame) { return false; }
		OVR::Mutex::Locker Locker(pMutex);
		OutFrame = Frame;
		bHasCapturedFrame = false;
		return true;
	}

	void Update()
	{
		if (!bHasCapturedFrame || !pImageBuffer) { return; }
		OVR::Mutex::Locker Locker(pMutex);

		D3D11_MAPPED_SUBRESOURCE map;
		Platform.Context->Map(pImageBuffer->Tex, 0, D3D11_MAP_WRITE_DISCARD, 0, &map);
		memcpy((void *)map.pData, (const void*)Frame.data, iBufferSize);
		Platform.Context->Unmap(pImageBuffer->Tex, 0);

	}

	void DrawLookThrough()
	{
		if (!pFadingEdgeQuadModel) { return; }

		Matrix4f mat = Matrix4f();
		Matrix4f proj = Matrix4f(); // OrthoProjection;
		proj.M[0][0] = fWebCamHMD_DiagonalFOVRatio;
		proj.M[1][1] = -fWebCamHMD_DiagonalFOVRatio*(bIsVertOriented ? 1.0f / fAspectRatio : fAspectRatio)*fHMDEyeAspectRatio;
		if (bIsVertOriented) { proj *= Matrix4f::RotationZ(-MATH_FLOAT_PIOVER2); }

		pFadingEdgeQuadModel->Fill->VShader->SetUniform("View", 16, (float *)&mat);
		pFadingEdgeQuadModel->Fill->VShader->SetUniform("Proj", 16, (float *)&proj);
		Platform.Context->OMSetBlendState(BlendState, NULL, 0xffffffff);

		Platform.Render(pFadingEdgeQuadModel->Fill, pFadingEdgeQuadModel->VertexBuffer, pFadingEdgeQuadModel->IndexBuffer,
			sizeof(Model::Vertex), pFadingEdgeQuadModel->numIndices);

		Platform.Context->OMSetBlendState(NULL, NULL, 0xffffffff);

	}

	static int CaptureFrameThreadFn(Thread *pthread, void* h)
	{
		OVR_UNUSED(pthread);
		WebCamDevice *pDevice = (WebCamDevice *)h;
		cv::Mat TmpRGBFrame;

		while (pDevice->iStatus == OVR::Thread::Running)
		{
			bool bSuccess = pDevice->Video.read(TmpRGBFrame);						
			if (bSuccess)
			{
				pDevice->ProcessHand(TmpRGBFrame);
				if (pDevice->pucRGBAData == NULL) { pDevice->pucRGBAData = (unsigned char*)OVR_ALLOC(pDevice->iBufferSize); }
				cv::Mat TmpRGBAFrame(TmpRGBFrame.size(), CV_8UC4, pDevice->pucRGBAData);
				cv::cvtColor(TmpRGBFrame, TmpRGBAFrame, CV_BGR2RGBA, pDevice->iColorChannels);
				pDevice->SetFrame(TmpRGBAFrame);

			}
			else { OVR_DEBUG_LOG(("Cannot read a frame from video file.")); }
		}
		return(1);
	}
};

//----------------------------------------------------------------------------------------------------------------------------------------
//  WebCamManager Class

class WebCamManager
{
public:

	WebCamDevice WebCams[WEBCAM_NB];

public:

	WebCamManager(ovrHmd HMD)
	{
		fHMDEyeAspectRatio = 0.5f*(float)HMD->Resolution.w / (float)HMD->Resolution.h;

#if WEBCAM_NB		
		WebCams[0].Initialize(WEBCAM_0_DEVICE_NUMBER, WEBCAM_0_VERT_ORIENTATION, WEBCAM_0_HMD_FOV_RATIO);
#endif
#if WEBCAM_NB == 2
		WebCams[1].Initialize(WEBCAM_1_DEVICE_NUMBER, WEBCAM_1_VERT_ORIENTATION, WEBCAM_1_HMD_FOV_RATIO);
#endif
	}

	void Update()
	{
#if WEBCAM_NB
		WebCams[0].Update();
#endif
#if WEBCAM_NB == 2
		WebCams[1].Update();
#endif
	}

	void DrawLookThrough(int eye)
	{
#if WEBCAM_NB == 1
		eye = 0;
#endif
		WebCams[eye].DrawLookThrough();
	}

	void StopCapture()
	{
#if WEBCAM_NB
		WebCams[0].StopCapture();
#endif
#if WEBCAM_NB == 2
		WebCams[1].StopCapture();
#endif
	}
};

//----------------------------------------------------------------------------------------------------------------------------------------

