/*This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

include <cyl_head_bolt.scad>;

module riftArm()
{
    difference()
    {
        union()
        {
            cube(size=[210,15,15],center=true);
            for (x=[-1,1])
            {
                translate([x*100,0,7.5])
                {
                   cube(size=[10,15,30],center=true); 
                }
            }
        }
        
        translate([0,0,12.5])
        {
            rotate([0,90,0])
            {
                cylinder(d=5.2,h=211,center=true,$fn=50);
            }
        }
        
        translate([0,0,4.76])
        {
            cube(size=[150,6.5,5.5],center=true);
            for (x=[-1,1])
            {
                translate([x*75,0,0])
                {
                   cylinder(d=6.5,h=5.5,center=true,$fn=50); 
                }
            }
        }
        
        translate([0,0,0])
        {
            cube(size=[150,5,15.1],center=true);
            for (x=[-1,1])
            {
                translate([x*75,0,0])
                {
                   cylinder(d=5,h=15.1,center=true,$fn=50); 
                }
            }
        }
        
        for (x=[-102.5,97.5])
        {
            translate([x,0,12.5])
            {
                rotate([0,-90,0])
                {
                    nutcatch_sidecut("M5", l=20, clk=0.1, clh=0.1, clsl=0.1);
                }
            }
        }
    }   
}


module riftClamp()
{
    difference()
    {
        cube(size=[20,10,5],center=true);
        
        translate([0,0,2.5])
        {
            cylinder(d=5,h=5,center=true,$fn=50);
        }
    }
}


module cameraArm()
{
    difference()
    {
        cube(size=[100,15,15],center=true); 
        
        translate([0,0,5.1])
        {
            cube(size=[60,2.5,5],center=true); 
        }
        
       for(x=[-1,1])
       {
           translate([x*40,0,0])
           {
               rotate([90,0,0])
               {
                    cylinder(d=5,h=16,center=true,$fn=50);
               }
           }
       }
    }
}
module riftSet()
{
    for (x=[-2,-1,1,2])
    {
        translate([x*25,0,2.5])
        {
            riftClamp();
        }
    }

    for (y=[-1,1])
    {
        translate([0,y*20,7.5])
        {
            riftArm();
        }
    }
    
    for (y=[-1,1])
    {
        translate([0,y*40,7.5])
        {
            cameraArm();
        }
    }
}



riftSet();
